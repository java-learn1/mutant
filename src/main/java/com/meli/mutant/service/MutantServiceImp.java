package com.meli.mutant.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.meli.mutant.model.nosql.DNADocument;
import com.meli.mutant.repository.DNARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

@Service
public class MutantServiceImp implements MutantService{

    @Autowired
    DNARepository dnaRepository;

    static final String chainDNAX_AAAA = "AAAA";
    static final String chainDNAX_CCCC = "CCCC";
    static final String chainDNAX_GGGG = "GGGG";
    static final String chainDNAX_TTTT = "TTTT";

    @Override
    public boolean isMutant(String[] dna) {
        boolean isMutant = false;

        long allChainX = 0;
        List<String> horizontalLines = getHorizontalLines(dna);
        allChainX += horizontalLines.stream().filter(isChainX()).count();

        List<String> verticalLines = getVerticalLines(dna);
        allChainX += verticalLines.stream().filter(isChainX()).count();

        List<String> diagonalLines = getDiagonalLines(dna);
        allChainX += diagonalLines.stream().filter(isChainX()).count();

        if(allChainX > 0L){
            isMutant = true;
        }

        return isMutant;
    }

    @Override
    public void registerDNASequence(DNADocument dnaDocument) {
        dnaRepository.save(dnaDocument);
    }

    @Override
    public ObjectNode stats() {
        List<DNADocument> dnaDocuments = dnaRepository.findAll();

        if (!dnaDocuments.isEmpty()){
            Long countMutants = dnaDocuments.stream().filter(isMutantDNA()).count();

            ObjectMapper mapper = new ObjectMapper();
            ObjectNode response = mapper.createObjectNode();

            response.put("count_mutant_dna", countMutants);
            response.put("count_human_dna", dnaDocuments.size()-countMutants);

            double ratio = (dnaDocuments.size()-countMutants == 0) ? 100.0 : countMutants/(dnaDocuments.size()-countMutants);
            response.put("ratio", ratio);

            return response;
        }else{
            return null;
        }
    }

    private static List<String> getHorizontalLines(String[] dna){
        return Arrays.asList(dna);
    }

    private static List<String> getVerticalLines(String[] dna){
        List<String> verticalLines = new ArrayList<>(dna.length);
        for (int i = 0; i < dna.length; i++) {
            StringBuilder verticalLine = new StringBuilder();
            for (String chainDNA : dna) {
                verticalLine.append(chainDNA.charAt(i));
            }
            verticalLines.add(verticalLine.toString());
        }
        return  verticalLines;
    }

    private static List<String> getDiagonalLines(String[] dna){
        List<String> diagonalLines = new ArrayList<>();

        StringBuilder diagonalFirst = new StringBuilder();
        StringBuilder diagonalSecond = new StringBuilder();

        for (int i = 0; i < dna.length; i++) {
            diagonalFirst.append(dna[i].charAt(i));
            diagonalSecond.append(dna[i].charAt((dna.length-1)-i));
        }
        diagonalLines.add(diagonalFirst.toString());
        diagonalLines.add(diagonalSecond.toString());
        return diagonalLines;
    }

    public static Predicate<String> isChainX(){
        return chainDNA -> (chainDNA.indexOf(chainDNAX_AAAA)>-1) ||
                (chainDNA.indexOf(chainDNAX_CCCC)>-1) ||
                (chainDNA.indexOf(chainDNAX_GGGG)>-1) ||
                (chainDNA.indexOf(chainDNAX_TTTT)>-1) ;
    }


    public static Predicate<DNADocument> isMutantDNA(){
        return dnaDocument -> dnaDocument.isMutant() == true;
    }
}
