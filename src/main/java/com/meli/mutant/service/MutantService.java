package com.meli.mutant.service;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.meli.mutant.model.nosql.DNADocument;

public interface MutantService {

    boolean isMutant(String[] dna);

    void registerDNASequence(DNADocument dnaDocument);

    ObjectNode stats();
}
