package com.meli.mutant.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DNARequest {
    @JsonProperty
    private String[] dna;

    public String[] getDna() {
        return dna;
    }

    public void setDna(String[] dna) {
        this.dna = dna;
    }
}
