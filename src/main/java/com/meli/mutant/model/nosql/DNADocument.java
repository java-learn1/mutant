package com.meli.mutant.model.nosql;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Document("dna")
public class DNADocument {
    @Id
    private String id;
    private List<String> sequence;
    private boolean isMutant;
    private LocalDateTime created;

    public DNADocument() {
        this.created = LocalDateTime.now();
        this.sequence = sequence = new ArrayList<>();
    }

    public List<String> getSequence() {
        return sequence;
    }

    public void setSequence(List<String> sequence) {
        this.sequence = sequence;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public boolean isMutant() {
        return isMutant;
    }

    public void setMutant(boolean mutant) {
        isMutant = mutant;
    }
}
