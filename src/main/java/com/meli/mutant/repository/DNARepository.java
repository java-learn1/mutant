package com.meli.mutant.repository;

import com.meli.mutant.model.nosql.DNADocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DNARepository extends MongoRepository<DNADocument, Integer> {
}
