package com.meli.mutant.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.meli.mutant.model.nosql.DNADocument;
import com.meli.mutant.model.request.DNARequest;
import com.meli.mutant.service.MutantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("/api/")
public class MutantController {

    @Autowired
    MutantService mutantService;

    @PostMapping("/mutant")
    public ResponseEntity<ObjectNode> isMutant(@RequestBody DNARequest dnaRequest) {
        boolean isMutant = mutantService.isMutant(dnaRequest.getDna());

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode response = mapper.createObjectNode();
        response.put("mutant", isMutant);

        DNADocument dnaDocument = new DNADocument();
        dnaDocument.setSequence(Arrays.asList(dnaRequest.getDna()));
        dnaDocument.setMutant(isMutant);

        mutantService.registerDNASequence(dnaDocument);
        return (isMutant) ? new ResponseEntity<>(response, HttpStatus.OK) : new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
    }

    @GetMapping("/stats")
    public ResponseEntity<ObjectNode> stats() {
        ObjectNode responseStats = mutantService.stats();
        return (responseStats != null) ? new ResponseEntity<>(responseStats, HttpStatus.OK) : new ResponseEntity<>(responseStats, HttpStatus.FORBIDDEN);
    }
}
